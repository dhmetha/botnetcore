---
website: https://etha.dev/
---

# Botnetcore

## 0.0.2 (2020-01-28)

### Redesign

- Switch webapi project

## 0.0.1 (2019-12-21)

### Initial

- New console bot
- Almanax daily reminder
