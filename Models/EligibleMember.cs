using System.Collections.Generic;
using botnetcore.Models.Mongo;

namespace botnetcore.Models
{
    class EligibleMember
    {
        public HouseholdMember Member { get; set; }
        public List<HouseholdChore> Chores { get; set; }
    }
}
