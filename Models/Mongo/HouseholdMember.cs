using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace botnetcore.Models.Mongo
{
    public class HouseholdMember
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("discordId")]
        public ulong DiscordId { get; set; }
    }
}
