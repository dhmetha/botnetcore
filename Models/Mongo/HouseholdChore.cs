using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace botnetcore.Models.Mongo
{
    public class HouseholdChore
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("interval")]
        public int Interval { get; set; }

        [BsonElement("notDoneSince")]
        public int NotDoneSince { get; set; }

        [BsonElement("lastMember")]
        public HouseholdMember LastMember { get; set; }
    }
}
