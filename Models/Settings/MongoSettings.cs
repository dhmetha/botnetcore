namespace botnetcore.Models.Settings {
    public class MongoSettings {
        public string Username { get; set; }
        public string Password { get; set; }
        public string IdentityDatabaseName { get; set; } = "admin";
        public string DatabaseName { get; set; }
        public string ServerAddress { get; set; }
        public int ServerPort { get; set; } = 27017;
    }
}
