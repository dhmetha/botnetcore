namespace botnetcore.Models.Settings
{
    public class AlmanaxSettings
    {
        public ulong GuildId { get; set; }
        public string Uri { get; set; }
    }
}
