namespace botnetcore.Models
{
    class Almanax
    {
        public string Description;
        public string ImgUrl;

        public Almanax WithDescription(string description)
        {
            Description = description;
            return this;
        }

        public Almanax WithImgUrl(string imgUrl)
        {
            ImgUrl = imgUrl;
            return this;
        }
    }
}
