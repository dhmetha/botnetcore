﻿using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace botnetcore.Controllers
{
    [ApiController]
    [Route("")]
    public class ChangelogController : ControllerBase
    {
        [HttpGet("")]
        [HttpGet("changelog")]
        public IActionResult Get()
            => PhysicalFile(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "changelog.html"), "text/HTML");
    }
}
