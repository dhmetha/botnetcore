﻿using botnetcore.Models.Mongo;
using botnetcore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace botnetcore.Controllers
{
    [ApiController]
    [Route("household")]
    public class HouseholdChoresController : ControllerBase
    {
        private readonly IMongoRepository _mongoRepo;
        public HouseholdChoresController(IMongoRepository mongoRepo)
        {
            _mongoRepo = mongoRepo;
        }

        [HttpGet("chores")]
        public IActionResult GetChores()
            => Ok(_mongoRepo.GetHouseholdChoresAsync());

        [HttpPost("chores")]
        public IActionResult PostChore(HouseholdChore chore)
            => Ok();

        [HttpGet("members")]
        public IActionResult GetMembers()
            => Ok(_mongoRepo.GetHouseholdMembersAsync());

        [HttpGet("members/{id}")]
        public IActionResult GetMemberByDiscordId(ulong id)
            => Ok(_mongoRepo.GetHouseholdMemberByDiscordIdAsync(id));

        [HttpPost("members")]
        public IActionResult PostMember(HouseholdMember member)
            => Ok();
    }
}
