using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace botnetcore.Helpers
{
    public class BotHelpers
    {
        public static int SecondInMs = 1000;
        public static int MinuteInMs = SecondInMs * 60;
        public static int HourInMs = MinuteInMs * 60;
        public static int DayInMs = HourInMs * 24;
        private static HttpClient _client = new HttpClient();
        public static int GetDelayUntilTomorrow()
        {
            var now = DateTime.Now;
            var tomorrow = DateTime.Today.AddDays(1);
            return (int) ((tomorrow.Ticks - now.Ticks) / TimeSpan.TicksPerMillisecond);
        }

        public static async Task<Color> GetDominantColorFromUrlAsync(string url)
        {
            var stream = await _client.GetStreamAsync(url);
            var image = Image.FromStream(stream);
            var bmp = new Bitmap(image);

            //Used for tally
            var colors = new List<Color>();

            for (var x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    var color = bmp.GetPixel(x, y);
                    colors.Add(color);
                }
            }

            return colors
                .GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .FirstOrDefault().FirstOrDefault();
        }
    }
}
