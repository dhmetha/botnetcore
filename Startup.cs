using botnetcore.Services;
using botnetcore.Services.Hosted;
using botnetcore.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using botnetcore.Models.Settings;
using Microsoft.OpenApi.Models;

namespace botnetcore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpClient();

            services.Configure<BotSettings>(Configuration.GetSection("BotSettings"));
            services.Configure<AlmanaxSettings>(Configuration.GetSection("AlmanaxSettings"));
            services.Configure<MongoSettings>(Configuration.GetSection("MongoSettings"));

            services.AddSingleton<IMongoRepository, MongoRepository>();
            services.AddSingleton<IBotnetcoreService, BotnetcoreService>();
            services.AddSingleton<IAlmanaxService, AlmanaxService>();
            services.AddSingleton<IHouseholdChoresServices, HouseholdChoresServices>();

            services.AddHostedService<BotnetcoreHostedService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Botnetcore", Version = "v0" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Botnetcore");
            });

            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
