using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using botnetcore.Models.Settings;
using botnetcore.Helpers;
using botnetcore.Services.Interfaces;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace botnetcore.Services.Hosted
{
    public class BotnetcoreHostedService : BackgroundService
    {
        private HttpClient _client;
        private ILogger<BotnetcoreHostedService> _logger;
        private readonly BotSettings _botSettings;
        private readonly IBotnetcoreService _botSvc;
        private readonly IAlmanaxService _almanaxSvc;
        private readonly IHouseholdChoresServices _choresSvc;

        public BotnetcoreHostedService(
            IHttpClientFactory clientFactory,
            ILogger<BotnetcoreHostedService> logger,
            IOptions<BotSettings> botSettings,
            IBotnetcoreService botSvc,
            IAlmanaxService almanaxSvc,
            IHouseholdChoresServices choresSvc
        ) {
            _client = clientFactory.CreateClient();
            _logger = logger;
            _botSettings = botSettings.Value;
            _botSvc = botSvc;
            _almanaxSvc = almanaxSvc;
            _choresSvc = choresSvc;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
            => InitDiscordBot();

        private async Task InitDiscordBot()
        {
            _botSvc.Bot = new DiscordSocketClient();

            _botSvc.Bot.Log += HandleLog;
            _botSvc.Bot.Ready += HandleReady;
            // _botSvc.Bot.MessageReceived += HandleMessageReceivedAsync;
            // _botSvc.Bot.GuildMemberUpdated += UpdateRoleColorFromMostFrequentAvatarColorOnGuildUserUpdateOnlyInBtsSioServer;

            await _botSvc.Bot.LoginAsync(TokenType.Bot, _botSettings.Token);
            await _botSvc.Bot.StartAsync();

            await Task.Delay(-1);
        }

        private async Task UpdateRoleColorFromMostFrequentAvatarColorOnGuildUserUpdateOnlyInBtsSioServer(SocketGuildUser before, SocketGuildUser after)
        {
            if (after.Guild.Id == 361898157627408384)
            {
                var dominantColor = await BotHelpers.GetDominantColorFromUrlAsync(after.GetAvatarUrl());
                var color = new Discord.Color(dominantColor.R, dominantColor.G, dominantColor.B);
                foreach (var role in after.Roles)
                {
                    if (role.Members.Count() == 1)
                    {
                        await role.ModifyAsync(x => x.Color = color);
                    }
                }
            }
        }

        private Task HandleLog(LogMessage message)
            => Task.Run(() => { _logger.LogInformation(message.ToString()); });

        private Task HandleReady()
        {
            Task.Run(_almanaxSvc.InitDailyAlmanax);
            Task.Run(_choresSvc.InitHouseholdChores);

            return Task.CompletedTask;
        }

        // Not currently used
        private Task HandleMessageReceivedAsync(SocketMessage messageParam)
            => Task.CompletedTask;
    }
}
