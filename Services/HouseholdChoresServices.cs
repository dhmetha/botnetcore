using System.Collections.Generic;
using System.Threading.Tasks;
using botnetcore.Helpers;
using botnetcore.Models;
using botnetcore.Models.Mongo;
using botnetcore.Services.Interfaces;
using Discord;
using Microsoft.Extensions.Logging;

namespace botnetcore.Services
{
    public class HouseholdChoresServices : IHouseholdChoresServices
    {
        private readonly ILogger<HouseholdChoresServices> _logger;
        private readonly IMongoRepository _mongoRepo;
        private readonly IBotnetcoreService _botSvc;
        public HouseholdChoresServices(
            ILogger<HouseholdChoresServices> logger,
            IMongoRepository mongoRepo,
            IBotnetcoreService botSvc
        ) {
            _logger = logger;
            _mongoRepo = mongoRepo;
            _botSvc = botSvc;
        }

        public void InitHouseholdChores()
        {
            var delay = BotHelpers.GetDelayUntilTomorrow();

            // adjust to send at 17h
            var delayDiff = BotHelpers.HourInMs * (24 - 17);
            if (delay > delayDiff) delay -= delayDiff;
            else delay += delayDiff;

            _logger.LogInformation($"Next daily chores in {delay}ms");
            Task.Delay(delay).ContinueWith(t => SendDailyChoresAsync());
        }

        private async Task SendDailyChoresAsync()
        {
            var eligibileMembers = await GetEligibleMembersAsync();

            foreach (var eligible in eligibileMembers)
            {
                var embedBuilder = new EmbedBuilder().WithTitle("Tâches du jour !");

                foreach (var chore in eligible.Chores)
                {
                    embedBuilder.AddField(chore.Name, chore.Description);
                }

                var embed = embedBuilder.Build();

                var user = _botSvc.GetDiscordUserByIdAsync(eligible.Member.DiscordId);
                if (!user.IsBot) await _botSvc.SendDmEmbedAsync(eligible.Member.DiscordId, embed);
            }

            _logger.LogInformation($"Next daily chores in {BotHelpers.DayInMs}ms");
            await Task.Delay(BotHelpers.DayInMs).ContinueWith(t => SendDailyChoresAsync());
        }

        private async Task<List<EligibleMember>> GetEligibleMembersAsync()
        {
            var members = await _mongoRepo.GetHouseholdMembersAsync();
            var chores = await _mongoRepo.GetHouseholdChoresAsync();

            var eligibileMembers = new List<EligibleMember>();

            foreach (var chore in chores)
            {
                if (chore.NotDoneSince < chore.Interval) continue;

                var lastMember = members.Find(x => x.Id == chore.LastMember.Id);
                var index = members.IndexOf(lastMember);

                var nextMember = new HouseholdMember();

                if (index + 1 >= members.ToArray().Length) nextMember = members[0];
                else nextMember = members[index+1];

                if (eligibileMembers.Find(x => x.Member == nextMember) != null)
                {
                    var foundIndex = eligibileMembers.FindIndex(x => x.Member == nextMember);
                    eligibileMembers[foundIndex].Chores.Add(chore);
                }
                else
                {
                    var eligibileMember = new EligibleMember();
                    eligibileMember.Chores.Add(chore);
                    eligibileMembers.Add(eligibileMember);
                }
            }

            return eligibileMembers;
        }
    }
}
