using System;
using System.Net.Http;
using System.Threading.Tasks;
using botnetcore.Helpers;
using botnetcore.Models;
using botnetcore.Models.Settings;
using botnetcore.Services.Interfaces;
using Discord;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace botnetcore.Services
{
    public class AlmanaxService : IAlmanaxService
    {
        private readonly ILogger<AlmanaxService> _logger;
        private readonly HttpClient _client;
        private readonly AlmanaxSettings _almanaxSettings;
        private readonly IBotnetcoreService _botSvc;
        public AlmanaxService(
            ILogger<AlmanaxService> logger,
            IHttpClientFactory clientFactory,
            IOptions<AlmanaxSettings> almanaxSettings,
            IBotnetcoreService botSvc
        ) {
            _logger = logger;
            _client = clientFactory.CreateClient();
            _almanaxSettings = almanaxSettings.Value;
            _botSvc = botSvc;
        }

        public void InitDailyAlmanax()
        {
            var delay = BotHelpers.GetDelayUntilTomorrow();

            _logger.LogInformation($"Next daily almanax in {delay}ms");
            Task.Delay(delay).ContinueWith(t => SendDailyAlmanaxAsync());
        }

        private async Task<Embed> GetCurrentAlmanaxEmbedAsync()
        {
            var almanax = await GetCurrentAlmanaxAsync();
            return new EmbedBuilder()
                .WithTitle("Almanax du jour !")
                .WithDescription(almanax.Description)
                .WithThumbnailUrl(almanax.ImgUrl)
                .Build();
        }

        private async Task SendDailyAlmanaxAsync()
        {
            var embed = await GetCurrentAlmanaxEmbedAsync();
            var guild = _botSvc.Bot.GetGuild(_almanaxSettings.GuildId);
            foreach (var user in guild.Users)
            {
                if (!user.IsBot) await _botSvc.SendDmEmbedAsync(user.Id, embed);
            }
            var delay = 1000 * 60 * 60 * 24;
            _logger.LogInformation($"Next daily almanax in {delay}ms");
            await Task.Delay(delay).ContinueWith(t => SendDailyAlmanaxAsync());
        }

        private async Task<Almanax> GetCurrentAlmanaxAsync()
        {
            var date = DateTime.Now.ToString("yyyy-MM-dd");
            var response = await _client.GetAsync(_almanaxSettings.Uri + date);
            var almanaxPage = await response.Content.ReadAsStringAsync();
            var almanaxDocument = new HtmlDocument();
            almanaxDocument.LoadHtml(almanaxPage);
            almanaxDocument.LoadHtml(
                almanaxDocument.DocumentNode.SelectSingleNode("(//div[contains(@id,'achievement_dofus')]//div[contains(@class,'more-infos-content')])[1]").InnerHtml
            );
            return new Almanax()
                .WithImgUrl(almanaxDocument.DocumentNode.SelectSingleNode("(//img)[1]").GetAttributeValue("src", ""))
                .WithDescription(almanaxDocument.DocumentNode.SelectSingleNode("(//p)[1]").InnerText.Trim());
        }
    }
}
