using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace botnetcore.Services.Interfaces
{
    public interface IBotnetcoreService
    {
        DiscordSocketClient Bot { get; set; }
        Task SendDmEmbedAsync(ulong to, Embed embed);
        SocketUser GetDiscordUserByIdAsync(ulong userId);
    }
}
