using System.Collections.Generic;
using System.Threading.Tasks;
using botnetcore.Models.Mongo;

namespace botnetcore.Services.Interfaces
{
    public interface IMongoRepository
    {
        Task<List<HouseholdChore>> GetHouseholdChoresAsync();
        Task PostHouseholdChoreAsync(HouseholdChore hChore);
        Task DeleteHouseholdChoreByIdAsync(string Id);
        Task DeleteHouseholdChoreByNameAsync(string Name);

        Task<List<HouseholdMember>> GetHouseholdMembersAsync();
        Task<HouseholdMember> GetHouseholdMemberByDiscordIdAsync(ulong DiscordId);
        Task PostHouseholdMemberAsync(HouseholdMember hMember);
        Task DeleteHouseholdMemberByIdAsync(string Id);
        Task DeleteHouseholdMemberByDiscordIdAsync(ulong DiscordId);
    }
}
