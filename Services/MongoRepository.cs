using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using botnetcore.Services.Interfaces;
using botnetcore.Models.Mongo;
using botnetcore.Models.Settings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace botnetcore.Services
{
    public class MongoRepository : IMongoRepository
    {
        private readonly ILogger<MongoRepository> _logger;
        private readonly IMongoCollection<HouseholdChore> _hChores;
        private readonly IMongoCollection<HouseholdMember> _hMembers;

        public MongoRepository(
            ILogger<MongoRepository> logger,
            IOptions<MongoSettings> mongoSettings
        ) {
            _logger = logger;
            var m = mongoSettings.Value;
            var mongoConnectionString = $"mongodb://{m.Username}:{m.Password}@{m.ServerAddress}:{m.ServerPort}/{m.IdentityDatabaseName}";
            _logger.LogInformation("Mongo connection string : " + mongoConnectionString);
            var mongoUrl = new MongoUrl(mongoConnectionString);
            var client = new MongoClient(mongoUrl);
            var database = client.GetDatabase(m.DatabaseName);
            _logger.LogWarning(database.ToString());
            _hChores = database.GetCollection<HouseholdChore>("household_chore");
            _hMembers = database.GetCollection<HouseholdMember>("household_member");

            var test = new HouseholdMember();
            test.DiscordId = 188581786031226889;
            test.Name = "test";
            _hMembers.InsertOneAsync(test);
        }

        public async Task<List<HouseholdChore>> GetHouseholdChoresAsync()
            => await (await _hChores.FindAsync(f => true)).ToListAsync();
        public async Task PostHouseholdChoreAsync(HouseholdChore hChore)
            => await _hChores.InsertOneAsync(hChore);
        public async Task DeleteHouseholdChoreByIdAsync(string Id)
            => await _hChores.DeleteOneAsync(f => f.Id == Id);
        public async Task DeleteHouseholdChoreByNameAsync(string Name)
            => await _hChores.DeleteOneAsync(f => f.Name == Name);

        public async Task<List<HouseholdMember>> GetHouseholdMembersAsync()
            => await (await _hMembers.FindAsync(f => true)).ToListAsync();
        public async Task<HouseholdMember> GetHouseholdMemberByDiscordIdAsync(ulong DiscordId)
            => await (await _hMembers.FindAsync(f => f.DiscordId == DiscordId)).FirstOrDefaultAsync();
        public async Task PostHouseholdMemberAsync(HouseholdMember hMember)
            => await _hMembers.InsertOneAsync(hMember);
        public async Task DeleteHouseholdMemberByIdAsync(string Id)
            => await _hMembers.DeleteOneAsync(f => f.Id == Id);
        public async Task DeleteHouseholdMemberByDiscordIdAsync(ulong DiscordId)
            => await _hMembers.DeleteOneAsync(f => f.DiscordId == DiscordId);
    }
}
