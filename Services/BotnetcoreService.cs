using System.Threading.Tasks;
using botnetcore.Services.Interfaces;
using Discord;
using Discord.WebSocket;

namespace botnetcore.Services
{
    public class BotnetcoreService : IBotnetcoreService
    {
        public DiscordSocketClient Bot { get; set; }

        public async Task SendDmEmbedAsync(ulong to, Embed embed)
        {
            var chan = await Bot.GetUser(to).GetOrCreateDMChannelAsync();
            await chan.SendMessageAsync(embed: embed);
        }

        public SocketUser GetDiscordUserByIdAsync(ulong userId)
            => Bot.GetUser(userId);
    }
}
